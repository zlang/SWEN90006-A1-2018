package swen90006.machine;

import java.util.List;
import java.util.ArrayList;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests {
//	// Any method annotated with "@Before" will be executed before each test,
//	// allowing the tester to set up some shared resources.
//	@Before
//	public void setUp() {
//	}
//
//	// Any method annotated with "@After" will be executed after each test,
//	// allowing the tester to release any shared resources used in the setup.
//	@After
//	public void tearDown() {
//	}
//
//	// Any method annotation with "@Test" is executed as a test.
//	@Test
//	public void aTest() {
//		// the assertEquals method used to check whether two values are
//		// equal, using the equals method
//		final int expected = 2;
//		final int actual = 1 + 1;
//		assertEquals(expected, actual);
//	}
//
//	@Test
//	public void anotherTest() {
//		List<String> list = new ArrayList<String>();
//		list.add("a");
//		list.add("b");
//
//		// the assertTrue method is used to check whether something holds.
//		assertTrue(list.contains("a"));
//	}
//
//	// Test test opens a file and executes the machine
//	@Test
//	public void aFileOpenTest() {
//		final List<String> lines = readInstructions("examples/array.s");
//		Machine m = new Machine();
//		assertEquals(m.execute(lines), 45);
//	}
//
//	// To test an exception, specify the expected exception after the @Test
//	@Test(expected = java.io.IOException.class)
//	public void anExceptionTest() throws Throwable {
//		throw new java.io.IOException();
//	}
//
//	// This test should fail.
//	// To provide additional feedback when a test fails, an error message
//	// can be included
//	@Test
//	public void aFailedTest() {
//		// include a message for better feedback
//		final int expected = 2;
//		final int actual = 1 + 2;
//		assertEquals("Some failure message", expected, actual);
//	}
//
//	// Read in a file containing a program and convert into a list of
//	// string instructions
//	private List<String> readInstructions(String file) {
//		Charset charset = Charset.forName("UTF-8");
//		List<String> lines = null;
//		try {
//			lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
//		} catch (Exception e) {
//			System.err.println("Invalid input file! (stacktrace follows)");
//			e.printStackTrace(System.err);
//			System.exit(1);
//		}
//		return lines;
//	}

	// Tests the off point for Register < 0
	@Test(expected = InvalidInstructionException.class)
	public void EC1() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("add R-1 R0");
		machine.execute(instruction);
	}

	// Tests the off point for Register > 31
	@Test(expected = InvalidInstructionException.class)
	public void EC2() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("sub R32 R0");
		machine.execute(instruction);
	}

	// Tests the off point for val < -65535
	@Test(expected = InvalidInstructionException.class)
	public void EC3() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("mov R0 -65536");
		machine.execute(instruction);
	}

	// Tests the off point for val > 65535
	@Test(expected = InvalidInstructionException.class)
	public void EC4() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("ldr R0 R1 65536");
		machine.execute(instruction);
	}

	// Tests the on point for Register < 0 and Last command == "RET"
	@Test
	public void EC5_OnPointRegisterLessThanMin() {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("ret R0");
		assertEquals(machine.execute(instruction), 0);
	}
	
	// Tests the on point for Register > 31 and Last command == "RET"
	@Test
	public void EC5_OnPointRegisterGreaterThanMax() {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("ret R31");
		assertEquals(machine.execute(instruction), 0);
	}

	// Test on point for val < -65535, Register < 0 and off point for Last command == "RET"
	@Test(expected = NoReturnValueException.class)
	public void EC6_OnPointValLessThanMin() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("str R0 -65535 R1");
		machine.execute(instruction);
	}
	
	// Test on point for val > 65535, Register > 31 and off point for Last command == "RET"
	@Test(expected = NoReturnValueException.class)
	public void EC6_OnPointValGreaterThanMax() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("jz R31 65535");
		machine.execute(instruction);
	}

	// Tests the off point for Register < 0
	@Test(expected = InvalidInstructionException.class)
	public void EC7() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("div R-1 R0 R1");
		instruction.add("ret R0");
		machine.execute(instruction);
	}

	// Tests the off point for Register > 31
	@Test(expected = InvalidInstructionException.class)
	public void EC8() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("jmp 1");
		instruction.add("ret R32");
		machine.execute(instruction);
	}

	// Test off point for val < -65535
	@Test(expected = InvalidInstructionException.class)
	public void EC9() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("jz R0 -65536");
		instruction.add("ret R0");
		machine.execute(instruction);
	}

	// Test off point for val > 65535
	@Test(expected = InvalidInstructionException.class)
	public void EC10() throws Throwable {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("mov R0 65536");
		instruction.add("jz R0 1");
		instruction.add("ret R0");
		machine.execute(instruction);
	}

	// Test on points for Register < 0, Register > 31, val < -65535, val > 65535 and Last command == "RET" 
	@Test
	public void EC11() {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("mov R0 -65535");
		instruction.add("mov R31 65535");
		instruction.add("MUL R1 R0 R31");
		instruction.add("ret R31");
		assertEquals(machine.execute(instruction), 65535);
	}

	// Test on point for Register < 0, Register > 31, val < -65535, val > 65535 and off point for Last command == "RET"
	@Test(expected = NoReturnValueException.class)
	public void EC12() {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("mov R0 -65535");
		instruction.add("mov R31 65535");
		instruction.add("jmp -5");
		machine.execute(instruction);
	}

	// Test on point for wrong syntax
	@Test(expected = InvalidInstructionException.class)
	public void EC13_On() {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("");
		instruction.add("abc");
		machine.execute(instruction);
	}
	
	// Test off point for wrong syntax
	@Test
	public void EC13_Off() {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("ret R0");
		machine.execute(instruction);
		assertEquals(machine.execute(instruction), 0);
	}
	
	// Test on point for list length == 0
	@Test(expected = NoReturnValueException.class)
	public void EC14_On() {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		machine.execute(instruction);
	}
	
	// Test off point for list length == 0
	@Test(expected = InvalidInstructionException.class)
	public void EC14_Off() {
		Machine machine = new Machine();
		List<String> instruction = new ArrayList<>();
		instruction.add("ABC AA");
		machine.execute(instruction);
	}
}
