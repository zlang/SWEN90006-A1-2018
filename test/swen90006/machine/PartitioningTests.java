package swen90006.machine;

import java.util.List;
import java.util.ArrayList;

import org.junit.*;

import static org.junit.Assert.*;

public class PartitioningTests {

		@Test(expected = InvalidInstructionException.class)
		public void EC1() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("add R-1 R0");
			machine.execute(instruction);
		}

		@Test(expected = InvalidInstructionException.class)
		public void EC2() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("sub R32 R0");
			machine.execute(instruction);
		}

		@Test(expected = InvalidInstructionException.class)
		public void EC3() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("mov R0 -65536");
			machine.execute(instruction);
		}

		@Test(expected = InvalidInstructionException.class)
		public void EC4() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("ldr R0 R1 65536");
			machine.execute(instruction);
		}

		@Test
		public void EC5() {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("ret R0");
			assertEquals(machine.execute(instruction), 0);
		}

		@Test(expected = NoReturnValueException.class)
		public void EC6() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("jmp -1");
			machine.execute(instruction);
		}
		
		@Test(expected = InvalidInstructionException.class)
		public void EC7() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("div R-1 R0 R1");
			instruction.add("ret R0");
			machine.execute(instruction);
		}

		@Test(expected = InvalidInstructionException.class)
		public void EC8() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("str R32 1 R0");
			instruction.add("ret R0");
			machine.execute(instruction);
		}

		@Test(expected = InvalidInstructionException.class)
		public void EC9() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("jz R0 -65536");
			instruction.add("ret R0");
			machine.execute(instruction);
		}

		@Test(expected = InvalidInstructionException.class)
		public void EC10() throws Throwable {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("mov R0 65536");
			instruction.add("jz R0 1");
			instruction.add("ret R0");
			machine.execute(instruction);
		}
 
		@Test
		public void EC11() {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("mov R0 -65535");
			instruction.add("mov R31 65535");
			instruction.add("mul R1 R0 R31");
			instruction.add("ret R0");
			assertEquals(machine.execute(instruction), -65535);
		}

		@Test(expected = NoReturnValueException.class)
		public void EC12() {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("mov R0 -65535");
			instruction.add("mov R31 65535");
			instruction.add("jmp 2");
			machine.execute(instruction);
		}

		@Test(expected = InvalidInstructionException.class)
		public void EC13() {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			instruction.add("");
			instruction.add("abc");
			machine.execute(instruction);
		}
		
		@Test(expected = NoReturnValueException.class)
		public void EC14() {
			Machine machine = new Machine();
			List<String> instruction = new ArrayList<>();
			machine.execute(instruction);
		}
}
